﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour {
	
	[SerializeField]
	public bool exitType = false;
	private GameObject audioManager;

	// Use this for initialization
	void Start () {
		audioManager = GameObject.Find ("AudioManager");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider collide){
		if (collide.CompareTag ("Player")) {
			if (exitType) {
				audioManager.GetComponent<FPSSoundScript> ().CalmMusic ();
			} else {
				audioManager.GetComponent<FPSSoundScript> ().ActionMusic ();
			}
		}
	}
}
