﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillStreak : MonoBehaviour {

	private int streak;
	private float time = 0;
	public int streaktime;
	public int streakammount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Debug.Log (streak);
		if (streak > 0) {
			time += Time.deltaTime;
			if (time > streaktime) {
				if (streak > streakammount) {
					GetComponent<MeshRenderer> ().enabled = true;
					GetComponent<AudioSource> ().Play ();
				}
				time = 0;
				streak = 0;
			}
			if (time > 1.8f && GetComponent<MeshRenderer> ().enabled) {
				GetComponent<MeshRenderer> ().enabled = false;
			}
		}
	}

	public void AugmentStreak(){ 
		streak += 1;
	}
}
