﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class FPSSoundScript : MonoBehaviour {

	public UnityEngine.Audio.AudioMixer mainMix;
	public List<AudioClip> BGM;
	public float[] songduration;
	private bool calm = true;
	private float elapsed;

	// Use this for initialization
	void Start () {
		AudioManager.AddTracks (2, gameObject);
		AudioManager.TrackSettings(0, mainMix,"Chill",0.5f,true);
		AudioManager.TrackSettings(1, mainMix,"Combat",0.5f,true);

		AudioManager.PlayMusic (0, BGM[0]);
		AudioManager.FadeInCaller (0, 0.1f, AudioManager.tracklist [0].trackvolume);
	}
	void Update(){
		elapsed += Time.deltaTime;
		//Debug.Log (elapsed);
		if (calm && elapsed > songduration [0]) {
			AudioManager.PlayMusic (0, BGM [0]);
			elapsed = 0;
		}
		if (!calm && elapsed > songduration [1]) {
			AudioManager.PlayMusic (1, BGM [1]);
			elapsed = 0;
		}
	}
	
	public void CalmMusic(){
	//	Debug.Log ("CalmMusic");
		AudioManager.StopMusic (0);
		AudioManager.StopMusic (1);
		AudioManager.PlayMusic (0, BGM [0]);
		calm = true;
	}

	public void ActionMusic(){
	//	Debug.Log ("ActionMusic");
		AudioManager.StopMusic (0);
		AudioManager.StopMusic (1);
		AudioManager.PlayMusic (1, BGM [1]);
		calm = false;

	}
}
