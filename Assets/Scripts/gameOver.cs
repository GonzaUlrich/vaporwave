﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOver : MonoBehaviour {
	bool activador=true;

	void OnEnable(){
		Time.timeScale = 0;
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;	
		if (activador) {
			gameObject.SetActive (false);
			activador = false;
		}
	}

	void OnDisable(){
		Time.timeScale = 1;
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;	
	}

}
