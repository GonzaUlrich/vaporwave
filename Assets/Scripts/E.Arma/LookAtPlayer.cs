﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {

	private GameObject player;
    public float smoothing = 20.0f;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	

	void Update () {
        //transform.LookAt (player.transform.position);

        Vector3 forward = player.transform.position - this.transform.position;
        forward.Normalize();
        forward.y = 0;

        Quaternion rot = Quaternion.LookRotation(forward, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime / smoothing);
    }
}
