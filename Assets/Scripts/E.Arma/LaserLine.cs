﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLine : MonoBehaviour {

	public GameObject punteria;

	// Use this for initialization
	void Start () {
		
		punteria = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
 
        	GetComponent<LineRenderer> ().SetPosition (0, transform.position);
        	GetComponent<LineRenderer> ().SetPosition (1, punteria.transform.position);

    }
}
