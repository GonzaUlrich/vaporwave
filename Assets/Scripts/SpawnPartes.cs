﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPartes : MonoBehaviour
{
	int maxList=5;
	bool oneMoreTime=false;
	public GameObject[] body;
	float timer=0;

	static List<GameObject>  bodyParts;

	void Start()
	{
		if(bodyParts == null)
		bodyParts = new List<GameObject>();
		
		for (int i = 0; i < 5; i++) {
			for (int a = 0; a < body.Length; a++) {
				GameObject objBody = (GameObject)Instantiate(body[a]);
				objBody.SetActive(false);
				bodyParts.Add(objBody);
			}
		}
	}

	void Update()
	{
		if (oneMoreTime) {
			for (int a = 0; a < 5; a++) {
				GameObject objBody = (GameObject)Instantiate(body[a]);
				objBody.SetActive(false);
				bodyParts.Add(objBody);
			}
			maxList += 1;
			oneMoreTime = false;
		}
	}

	public void Dead(){

		for (int i = 0; i < bodyParts.Count; i++)
		{
			if (!bodyParts[i].activeInHierarchy)
			{
				bodyParts[i].transform.position = transform.position + new Vector3(0,1,0);
				bodyParts[i].transform.rotation = transform.rotation;
				bodyParts[i].SetActive(true);
				if (i>=maxList) {
					oneMoreTime = true;
				}
				break;
			}
		}
	}
}