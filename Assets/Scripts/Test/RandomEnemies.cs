﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnemies : MonoBehaviour {


	public GameObject[] spawn;
	public GameObject[] enemys;
	public GameObject spawnBoss;
	public GameObject bigBoss;
    private int cantEnemys = 4;
    private int cantEnemysBoss = 2;
    public int cantidad;
	private int contador;
    private int _piso;
	int boss=1;

	void OnEnable () {
		contador = 5;
		_piso = Teleporter.piso;
		cantidad = _piso + 2;
		for (int i = 0; i < spawn.Length; i++) {
			spawn [i].SetActive (true );
		}
	}

	void Update () {
        //Invocacion del boss
        if ((_piso%5)==0)
        {
            //Boss solo
            if (_piso<9)
            {
                bigBoss.transform.position = spawnBoss.transform.position;
                Instantiate(bigBoss);
                gameObject.SetActive(false);
            }
            //Boss con muchachos
            else
            {
                bigBoss.transform.position = spawnBoss.transform.position;
                Instantiate(bigBoss);
                for (int i = 0; i < cantEnemysBoss; i++)
                {
                    int randomSpanw = Random.Range(0, spawn.Length);
                    int randomEnemy = Random.Range(0, enemys.Length);
                    enemys[randomEnemy].transform.position = spawn[randomSpanw].transform.position;
                    Instantiate(enemys[randomEnemy]);
                    spawn[randomSpanw].SetActive(false);
                    
                }
                cantEnemysBoss += 1;
                gameObject.SetActive(false);
            }
            
        }
        else
        {
            for (int i = 0; i < cantEnemys; i++)
            {
                int randomSpanw = Random.Range(0, spawn.Length);
                int randomEnemy = Random.Range(0, enemys.Length);
                enemys[randomEnemy].transform.position = spawn[randomSpanw].transform.position;
                Instantiate(enemys[randomEnemy]);
                spawn[randomSpanw].SetActive(false);

            }
            cantEnemys += Random.Range(0, 2);

            if (_piso<10 && cantEnemys>9)
            {
                cantEnemys = 9;
            }
            gameObject.SetActive(false);
        }
        /*
        if (contador!=cantidad){
			

			if (contador!=0) {
				//boss = contador % 5;	
			}
			if (Teleporter.piso % 5 == 0 ) {
				
			}
			if (spawn[randomSpanw].activeInHierarchy ) {
				
				contador += 1;
			}
		} */
		
	}
}
// en Cuando toca el TP lo activa