﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPlayer : MonoBehaviour {

	public float speed = 6.0f;
	public float jumpSpeed=8.0f;
	public float gravity = 20.0f;
	private Vector3 moveDirection = Vector3.zero;
	public GameObject camara;
    CharacterController controller;


    void Awake(){
		camara = GameObject.FindGameObjectWithTag ("MainCamera");
        controller = GetComponent<CharacterController>();
    }

	void Update () {

		transform.rotation = camara.transform.rotation;

		if (Input.GetButton("Sprint")) {
			speed = 12.0f;
		} else {
			speed = 6.0f;
		}
		if (controller.isGrounded) {
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;

		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);
	}
}
