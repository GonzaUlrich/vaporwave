﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraRotate : MonoBehaviour
{

	Vector2 mouseLook;
	Vector2 SmoothV;
	public float sensitivity = 5.0f;
	public float smoothing =2.0f;

	int daño=1;
	float timer = 0f;

	GameObject player;
	public GameObject gameOverr;

	public Camera fpsCam;
	public ParticleSystem flash;
	public GameObject impact;


	void Update()
	{
		timer += Time.deltaTime;
		if (Input.GetButton("Fire1") && timer>=0.05f) {
			flash.Emit(1);
			RaycastHit hit;
			GetComponent<AudioSource> ().Play ();
			flash.Play();
			if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit)) {
				enemyhitbox tocado = hit.transform.GetComponent<enemyhitbox> ();
				BossHitbox todaco = hit.transform.GetComponent<BossHitbox> ();
				if (tocado!=null) {
					tocado.TakeDamage(daño);
				}
				if (todaco != null) {
					todaco.TakeDamage (daño);
				}
			}
			timer = 0f;
			GameObject impacto=Instantiate (impact, hit.point, Quaternion.LookRotation(hit.normal));
			Destroy (impacto,0.1f);

		}
	}
}