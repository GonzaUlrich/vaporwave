﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
	int maxList=9;
	bool oneMoreTime=false;
    public GameObject bullet;
	float timer=0;

    AudioSource bulletAudio;

    List<GameObject> bulletList;

    void Start()
    {
        bulletList = new List<GameObject>();
        for (int i = 0; i < 10; i++)
        {
            GameObject objBullet = (GameObject)Instantiate(bullet);
            objBullet.SetActive(false);
            bulletList.Add(objBullet);
        }
    }
		
    void FixedUpdate()
    {
		
		timer += Time.deltaTime;
		if (Input.GetButton("Fire1")&& timer>0.05f)
        {
            Fire();
			timer = 0;

        }
		if (oneMoreTime) {
			GameObject objBullet = (GameObject)Instantiate(bullet);
			objBullet.SetActive(false);
			bulletList.Add(objBullet);
			maxList += 1;
			oneMoreTime = false;
		}
    }

	void Fire(){

		for (int i = 0; i < bulletList.Count; i++)
		{
			if (!bulletList[i].activeInHierarchy)
			{
				bulletList[i].transform.position = transform.position;
				bulletList[i].transform.rotation = transform.rotation;
				bulletList[i].SetActive(true);
				Rigidbody tempRigidBodyBullet = bulletList[i].GetComponent<Rigidbody>();
				float bulletSpeed = 600000;
				tempRigidBodyBullet.AddForce(tempRigidBodyBullet.transform.forward * bulletSpeed * Time.fixedDeltaTime);
				if (i>=maxList) {
					oneMoreTime = true;
				}
				break;
			}
		}
	}
}
