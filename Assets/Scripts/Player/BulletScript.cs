﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    private void OnEnable()
    {
        transform.GetComponent<Rigidbody>().WakeUp();
        Invoke("hideBullet", 6.0f);
    }
		
    private void OnDisable()
    {
        transform.GetComponent<Rigidbody>().Sleep();
        CancelInvoke();
    }
		
   void hideBullet()
	{
		gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider coll){
		
			gameObject.SetActive (false);
			
		
	}
}