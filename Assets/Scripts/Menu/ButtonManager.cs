﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

	public GameObject flecha;
	public GameObject[] botones;
	public GameObject canvMenu;
	public Text txtHiscore;
	int numeroDeBoton=0;
	float timer=0;
	public AudioMixer audioMixer;
	public GameObject canvOpciones;
	int highscore=0;

	void Start(){
		/*Scene currentScene = SceneManager.GetActiveScene ();
		if (!File.Exists ("HS.txt")) {
			FileStream fs = File.Create ("HS.txt");
			StreamWriter sw = new StreamWriter (fs);
			sw.WriteLine ("0");
			sw.Close ();
			fs.Close ();
		}

		FileStream fs1 = File.OpenRead ("HS.txt");
		StreamReader sr = new StreamReader (fs1);
		highscore = int.Parse (sr.ReadLine ());
		fs1.Close ();
		sr.Close ();
*/
		txtHiscore.text = System.Convert.ToString(PlayerPrefs.GetInt ("Highscore", 0));


	}

	void Update(){



		Vector3 analogicoIzquierdo = Vector3.zero;
		analogicoIzquierdo.x = Input.GetAxis ("RightJoystickHorizontal");

		Vector3 analogicoDerecho = Vector3.zero;
		analogicoDerecho.x = Input.GetAxis ("LeftJoystickHorizontal");

		if (canvMenu.activeInHierarchy) {
		//	Debug.Log ("aa");
			//|| Input.GetKeyDown ("space")
			if (Input.GetButton ("Fire1")|| Input.GetKeyDown ("enter") ) {
				if (numeroDeBoton == 0) {
					StartBttn ();
				}
				if (numeroDeBoton == 1) {
					Options ();
				}
				if (numeroDeBoton == 2) {
					ExitGameBtn ();
				}
				timer += Time.deltaTime;
			}
			if (Input.GetKeyDown("jump")){	//analogicoIzquierdo.x <= -1f && timer > 0.2f || analogicoDerecho.x <= -1f) {
					//Debug.Log ("b");
					//numeroDeBoton += 1;
					//PFlecha ();

					if (numeroDeBoton == 0) {
						numeroDeBoton = 2;

					} else {
						numeroDeBoton -= 1;
						PFlecha ();
					}
					timer = 0;
				}
				/*if (analogicoIzquierdo.x >= 1f && timer > 0.2f || analogicoDerecho.x >= 1f && timer > 0.2f) {
					if (numeroDeBoton == 2) {
						numeroDeBoton = 0;
						PFlecha ();
					} else {
						numeroDeBoton += 1;
						PFlecha ();
					}
					timer = 0;
				}*/
			
		} else {
			if (Input.anyKey) {
				canvMenu.SetActive (true);
				canvOpciones.SetActive (false);
			}
		}
	}
	void PFlecha(){
		flecha.transform.position =new Vector3(flecha.transform.position.x,botones [numeroDeBoton].transform.position.y,flecha.transform.position.z) ;
	}

	public void StartBttn(){
		SceneManager.LoadScene ("Main");
	}

	public void Menu (){
		SceneManager.LoadScene ("Menu");
	}

	public void Options(){
		if (canvMenu.activeInHierarchy) {
			canvMenu.SetActive (false);
			canvOpciones.SetActive (true);
		} 
			
		
	}
	public void ExitGameBtn(){
		Application.Quit ();
	}
	public void	SetVolume(float volume){
		audioMixer.SetFloat ("volume",volume);
	}
	//PA EL FUTURO BROOO

/*	public void	TwitterBtn(){
		System.Diagnostics.Process.Start("");
	}
	public void	FacebookBtn(){
		System.Diagnostics.Process.Start("");
	}*/

}
