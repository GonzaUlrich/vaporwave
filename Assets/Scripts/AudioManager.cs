﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

	public static List<TrackClass> tracklist = new List<TrackClass> ();
	static private bool keepFadingIn;
	static private bool keepFadingOut;
	public static AudioManager instance;

	void Awake(){
		if (tracklist != null)
			tracklist = new List<TrackClass> ();
		if (instance != null)
			instance = new AudioManager ();
		
		instance = this;
	}

	static public void AddTracks(int numberOfTracks, GameObject gameObj){
		if (numberOfTracks != 0) {
			for (int i = 0; i < numberOfTracks; i++) {
				TrackClass track = new TrackClass{ id = 1, audiosource = gameObj.AddComponent<AudioSource> () };
				tracklist.Add (track);
			}
		}

	}

	static public void TrackSettings(int track, AudioMixer mainMix, string audioGroup,float trackVolume, bool loop = true){
		tracklist [track].audiosource.outputAudioMixerGroup = mainMix.FindMatchingGroups (audioGroup) [0];
		tracklist [track].trackvolume = trackVolume;
		tracklist [track].loop = loop;
	}

	static public void PlayMusic (int track, AudioClip audioClip){
		tracklist [track].audiosource.PlayOneShot (audioClip, tracklist [track].trackvolume);
	}
	static public void StopMusic ( int track){
		tracklist [track].audiosource.Stop ();
	}

	public static void FadeInCaller(int track, float speed, float maxVolume){
		instance.StartCoroutine(FadeIn (track, speed, maxVolume));
	}

	public static void FadeOutCaller(int track, float speed){
		instance.StartCoroutine(FadeOut (track, speed));
	}

	static IEnumerator FadeIn(int track, float speed, float maxvolume){
		keepFadingIn = true;
		keepFadingOut = false;

		tracklist [track].audiosource.volume = 0;
		float audioVolume = tracklist [track].audiosource.volume;
		while (tracklist [track].audiosource.volume < maxvolume && keepFadingIn) {
			audioVolume += speed;
			tracklist [track].audiosource.volume = audioVolume;
			yield return new WaitForSeconds (0.1f);
		}
	}

	static IEnumerator FadeOut (int track, float speed){
		keepFadingIn = false;
		keepFadingOut = true;

		float audioVolume = tracklist [track].audiosource.volume;

		while (tracklist [track].audiosource.volume >= speed && keepFadingOut) {
			audioVolume -= speed;
			tracklist [track].audiosource.volume = audioVolume;
			yield return new WaitForSeconds (0.1f);
		}
	}

	/*void start(){
		if (GetComponent<AudioSource> () == null) {
			gameObject.AddComponent<AudioSource> ();
		}
	}*/
}
