﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuMain : MonoBehaviour {

	public GameObject flecha;
	public GameObject[] botones;
	int numeroDeBoton=0;
	float timer=0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 analogicoIzquierdo = Vector3.zero;
		analogicoIzquierdo.x = Input.GetAxis ("RightJoystickHorizontal");

		Vector3 analogicoDerecho = Vector3.zero;
		analogicoDerecho.x = Input.GetAxis ("LeftJoystickHorizontal");

		if (Input.GetButton ("Fire1") || Input.GetKeyDown ("enter") || Input.GetKeyDown ("space")) {
			if (numeroDeBoton == 0) {
				Menu ();
			}
			if (numeroDeBoton == 1) {
				StartBttn ();
			}
			timer += Time.deltaTime;

			if (analogicoIzquierdo.x <= -1f && timer > 0.2f || analogicoDerecho.x <= -1f && timer > 0.2f) {
				if (numeroDeBoton == 0) {
					numeroDeBoton = 2;
					PFlecha ();
				} else {
					numeroDeBoton -= 1;
					PFlecha ();
				}
				timer = 0;
			}
			if (analogicoIzquierdo.x >= 1f && timer > 0.2f || analogicoDerecho.x >= 1f && timer > 0.2f) {
				if (numeroDeBoton == 2) {
					numeroDeBoton = 0;
					PFlecha ();
				} else {
					numeroDeBoton += 1;
					PFlecha ();
				}
				timer = 0;
			}
	}
	}
	void PFlecha(){
		flecha.transform.position =new Vector3(flecha.transform.position.x,botones [numeroDeBoton].transform.position.y,flecha.transform.position.z) ;
	}
	public void StartBttn(){
		SceneManager.LoadScene ("Main");
	}

	public void Menu (){
		SceneManager.LoadScene ("Menu");
	}
	public void Options(){
	
	}
}