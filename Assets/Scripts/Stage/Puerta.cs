﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour {

	public GameObject puerta1;
	public GameObject puerta2;
	public GameObject RoomClear;
	public GameObject spawn;

	void OnTriggerEnter(Collider col){

		if (col.tag=="Player") {
			puerta1.SetActive (true);
			puerta2.SetActive (true);
			spawn.SetActive (true);
			RoomClear.SetActive (true);
			gameObject.SetActive (false);
		}

	}
}