﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapas : MonoBehaviour {

	public GameObject[] rooms;
	public GameObject teleport;
	public new Vector3[] posicionTeleport;
    private GameObject[] decors;
    private GameObject[] floors;

	void OnEnable () {

        decors = GameObject.FindGameObjectsWithTag("Decor");
        floors = GameObject.FindGameObjectsWithTag("Piso");

		int randomRoom = Random.Range (0, rooms.Length);
		if (!rooms[randomRoom].activeSelf) {
			rooms [randomRoom].SetActive (true);
		} 

		for (int i = 0; i < rooms.Length; i++) {
			if (i==randomRoom) {
				teleport.transform.localPosition = posicionTeleport [randomRoom];
			}else{
				rooms [i].SetActive (false);
			}
		}

		gameObject.SetActive (false);
	}

    public GameObject[] GetDecor()
    {
        return decors;
    }

    public GameObject[] GetFloors()
    {
        return floors;
    }
}
