﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {


	public GameObject Destination;
	public GameObject activadorRoom1;
	public GameObject activadorRoom2;
	public GameObject map;
	static public int piso;

	void OnEnable(){
		piso = 1;
	}

	void OnTriggerEnter(Collider collision){
		if(collision.gameObject.tag == "Player"){
			collision.gameObject.transform.SetPositionAndRotation (Destination.transform.position, collision.gameObject.transform.rotation);
			activadorRoom1.SetActive (true);
			activadorRoom2.SetActive (true);
			piso += 1;
		}
		map.SetActive (true);
	}
}

//staircase -66.46429 -4.098457 -32.36859
//			-66.46429 -4.098457 6.4