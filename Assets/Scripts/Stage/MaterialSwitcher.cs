﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwitcher : MonoBehaviour {

    public bool decorations = false;
    public bool floors = false;
    public GameObject randomMapa;
	public Material passive;
	public Material[] actives;
	public bool Exit;
    private GameObject[] objetos;

	// Use this for initialization
	void Start () {
        Mapas maps;
        if((maps = randomMapa.GetComponent<Mapas>()) != null)
        {
            if (decorations)
            {
                objetos = maps.GetDecor();
            }
            if (floors)
            {
                objetos = maps.GetFloors();
            }
        }
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider collide) {
        int mat = Random.Range(0, actives.Length-1);
		if (collide.CompareTag ("Player")) {
			if (!Exit) {
                for (int i = 0; i < objetos.Length; i++)
                {
                    objetos[i].GetComponent<Renderer>().material = actives[mat];
                }
			} else {
                for (int i = 0; i < objetos.Length; i++)
                {
                    objetos[i].GetComponent<Renderer>().material = passive;
                }
            }
		}
	}
}
