﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomClear : MonoBehaviour {

	public GameObject puerta1;
	public GameObject puerta2;
	GameObject[] enemigos;

	int cantEnemy;

	void Update () {
		cantEnemy=GameObject.FindGameObjectsWithTag ("Enemy").Length;
		if (cantEnemy==0) {
			puerta1.SetActive (false);
			puerta2.SetActive (false);
			gameObject.SetActive (false);
		}

	}
	
}
