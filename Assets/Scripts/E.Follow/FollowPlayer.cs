﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	GameObject player;
	public Rigidbody rb;
	public int velocidad = 10 ;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("MainCamera");
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 lookAtPosition = player.transform.position;
		lookAtPosition.y = transform.position.y;
		transform.LookAt (lookAtPosition);
		//transform.LookAt (player.transform.position);
		rb.AddForce (transform.forward * velocidad);

	}
}
