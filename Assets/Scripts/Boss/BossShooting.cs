﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShooting : MonoBehaviour {

	int maxList=9;
	bool oneMoreTime=false;
	public float bulletSpeed = 1000;
	public GameObject bullet;
	float timer=0;

	AudioSource bulletAudio;

	List<GameObject> bulletList;

	void Start()
	{
		bulletList = new List<GameObject>();
		for (int i = 0; i < 10; i++)
		{
			GameObject objBullet = (GameObject)Instantiate(bullet);
			objBullet.SetActive(false);
			bulletList.Add(objBullet);
		}
	}

	void Update()
	{

		timer += Time.deltaTime;
		//Debug.Log (timer);
		if ( timer>2f)
		{
			Fire();
			timer = 0;
		}
		if (oneMoreTime) {
			GameObject objBullet = (GameObject)Instantiate(bullet);
			objBullet.SetActive(false);
			bulletList.Add(objBullet);
			maxList += 1;
			oneMoreTime = false;
		}
	}

	void Fire(){

		for (int i = 0; i < bulletList.Count; i++)
		{
			if (!bulletList[i].activeInHierarchy)
			{
				bulletList[i].transform.position = transform.position + transform.forward;
				bulletList[i].transform.rotation = transform.rotation;
				bulletList[i].SetActive(true);
				Rigidbody tempRigidBodyBullet = bulletList[i].GetComponent<Rigidbody>();
				tempRigidBodyBullet.AddForce(tempRigidBodyBullet.transform.forward * bulletSpeed);
				if (i>=maxList) {
					oneMoreTime = true;
				}
				GetComponent<AudioSource> ().Play ();
				break;
			}
		}
	}
}
