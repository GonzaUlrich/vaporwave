﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossHitbox : MonoBehaviour {
	
	int maxList = 3;
	private GameObject KS;
	public GameObject healthbar;
    public GameObject model;
    private float time;
    private SkinnedMeshRenderer srenderer;
    private MeshRenderer mrenderer;
    public Material normal;
    public Material onHit;


    public int hp=10;

	void Start(){
		KS = GameObject.FindGameObjectWithTag ("StreakMessage");
        srenderer = model.GetComponent<SkinnedMeshRenderer>();
        mrenderer = model.GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        if (time < 0)
        {
            time = 0;
            if (srenderer != null)
                srenderer.material = normal;
            else if (mrenderer != null)
                mrenderer.material = normal;
        }
        else if (time > 0)
        {
            time -= Time.deltaTime;
        }
    }

    public void TakeDamage(int daño){
		hp -= daño;

		if (hp<=0) {

			//GetComponent<SpawnPartes> ().Dead ();
			KS.GetComponent<KillStreak> ().AugmentStreak ();
			Destroy (transform.parent.gameObject);
		}
        time = 0.1f;
        if (srenderer != null)
            srenderer.material = onHit;
        else if (mrenderer != null)
            mrenderer.material = onHit;
        healthbar.transform.localScale -= new Vector3 (0.01f, 0, 0);
	}
	public int GetHealth(){
		return hp;
	}


}