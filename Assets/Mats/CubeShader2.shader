﻿Shader "Unlit/CubeShader"
{
	properties
	{
		_MainTexture("Texture",2D) = "white"{}
		_Color("Color",Color) = (1,1,1)
		_ModelColor("Model Color",Color) = (0,0,0)
		_Size("Size", Range(0.0001,1.0)) = 0.1
		[Toggle] _Hide("Hide", Float) = 0
		[Toggle] _ShowModel("Show Model", Float) = 0
		_Transparency("Transparency",Range(0.0,1.0)) = 0.25
	}
		SubShader
	{

		///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///
		pass
		///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///PASS N1///
		{
			CGPROGRAM

			#pragma vertex vertexModel
			#pragma fragment fragmentModel
			#pragma geometry geo

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : COLOR0;
		};

			struct v2f
		{
			float4 position :SV_POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : COLOR0;
		};

		fixed4 _ModelColor;
		sampler2D _MainTexture;
		int _ShowModel;
		int _Hide;

			v2f vertexModel (appdata IN)
			 {
				 v2f OUT;
					if(_ShowModel==1)
					{
				 	OUT.position = UnityObjectToClipPos(IN.vertex);
				 	OUT.uv = IN.uv;
				 	OUT.normal =  _ModelColor;
					}

				 return OUT;
			 }

			 [maxvertexcount(3)]
			 void geo(triangle v2f IN[3], inout TriangleStream<v2f> pstream )
			 {

				 if(!_ShowModel || _Hide)
				 return;

				 v2f gOut;

				 for(int i = 0; i<3;i++)
				 {
					 gOut.position = IN[i].position;
					 gOut.uv = IN[i].uv;
					 gOut.normal = _ModelColor;
					 pstream.Append(gOut);
				 }

				 pstream.RestartStrip();

			 }

			 fixed4 fragmentModel (v2f IN, uniform float nutkin = 0.0f) : SV_Target
			 {
				if(_ShowModel==1)
				{
					fixed4 pixelcolor = tex2D(_MainTexture, IN.uv);
					pixelcolor *= _ModelColor;
					return pixelcolor;
				} else {
					return (fixed4(0,0,0,0));
				}
			 }

			 ENDCG
		}
		
		///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///
		pass
		///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///PASS N2///
	{
		
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}

			Blend SrcAlpha OneMinusSrcAlpha
		

		CGPROGRAM

#pragma vertex vertexShader
#pragma fragment fragmentShader
#pragma geometry geo

#define SIZE 39

#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : COLOR0;
		};

		struct geometryOutput
		{
			float4 pos:SV_POSITION;
		};

		struct v2f
		{
			float4 position :SV_POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : COLOR0;
		};

		fixed4 _Color;
		sampler2D _MainTexture;
		float _Size;
		int _Hide;
		float _Transparency;
		float _ShowModel;
		

		v2f vertexShader(appdata IN)
		{
			v2f OUT;
			
			if(_Hide==1)
			return OUT;

			OUT.position = IN.vertex;
			OUT.uv = IN.uv;
			OUT.normal = IN.normal;


			return OUT;
		}

		[maxvertexcount(SIZE)]
		void geo (triangle float4 IN[3] : SV_POSITION, inout TriangleStream<geometryOutput> triStream)
		{
			

			geometryOutput o;

			if(_Hide==1)
			return;

			float3 pos = IN[0];
			float size = _Size;

			// Update each assignment of o.pos.
			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);

			//--------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, -size));
			triStream.Append(o);

			//---------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, -size));
			triStream.Append(o);

			//----------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, -size));
			triStream.Append(o);

			//-----------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, size));
			triStream.Append(o);

			//------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, size));
			triStream.Append(o);

			//-------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, size));
			triStream.Append(o);

			//---------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, -size, -size));
			triStream.Append(o);

			//---------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);
			
			o.pos = UnityObjectToClipPos(pos + float3(-size, size, size));
			triStream.Append(o);

			//----------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, size));
			triStream.Append(o);

			//----------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, -size));
			triStream.Append(o);

			//---------------------------------------------------------

			o.pos = UnityObjectToClipPos(pos + float3(-size, size, -size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, size));
			triStream.Append(o);

			o.pos = UnityObjectToClipPos(pos + float3(-size, -size, -size));
			triStream.Append(o);

		}

		fixed4 fragmentShader(v2f IN, uniform float nutkin = 0.0f) : SV_Target
		{

			if(_Hide==1)
			return (fixed4(0,0,0,0));

			fixed4 pixelcolor = _Color;
			pixelcolor.a = _Transparency;

			return pixelcolor;
		}

			ENDCG
	}
		

	}
}
