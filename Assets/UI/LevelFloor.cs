﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelFloor : MonoBehaviour {

	public Text floorLevel;

	void Start () {
		
	}

	void Update () {
		if (Teleporter.piso==0) {
			floorLevel.text = "GF";//Ground Flor
		} else {
			floorLevel.text = Teleporter.piso.ToString();	
		}
	}
}
